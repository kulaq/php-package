<?php

namespace Php\Package\Tests\PolymorphismTests;

use Php\Package\Polymorphism\GetLinks;
use PHPUnit\Framework\TestCase;

class GetLinksTest extends TestCase
{
    public function testGetLinks1()
    {

        $tags = [
            ['name' => 'p'],
            ['name' => 'a', 'href' => 'hexlet.io'],
            ['name' => 'img', 'src' => 'hexlet.io/assets/logo.png'],
        ];
        $links = new GetLinks();
        $links1 = $links->getLinks($tags);

        $expected = [
            'hexlet.io',
            'hexlet.io/assets/logo.png'
        ];
        $this->assertEquals($expected, $links1);
    }

    public function testGetLinks2()
    {
        $tags = [];
        $links = new GetLinks();
        $links1 = $links->getLinks($tags);

        $expected = [];
        $this->assertEquals($expected, $links1);
    }

    public function testGetLinks3()
    {
        $tags = [
            ['name' => 'img', 'src' => 'hexlet.io/assets/logo.png'],
            ['name' => 'div'],
            ['name' => 'link', 'href' => 'hexlet.io/assets/style.css'],
            ['name' => 'h1']
        ];
        $links = new GetLinks();
        $links1 = $links->getLinks($tags);

        $expected = [
            'hexlet.io/assets/logo.png',
            'hexlet.io/assets/style.css'
        ];
        $this->assertEquals($expected, $links1);
    }

}