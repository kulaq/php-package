<?php

//Реализуйте функцию swapKeyValue(), которая принимает на вход объект
// базы данных и меняет в нём ключи и значения местами.

//swapKeyValue — полиморфная функция, она может работать с
// любой реализацией key-value, имеющей такой же интерфейс.

function swapKeyValue($map)
{
    $data = $map->toArray();
    array_walk($data, fn($value, $key) => $map->unset($key));
    array_walk($data, fn($value, $key) => $map->set($value, $key));
}