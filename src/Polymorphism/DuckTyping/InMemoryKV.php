<?php

namespace Php\Package\Polymorphism\DuckTyping;
//6. Полиморфизм и утиная типизация
//Реализуйте класс InMemoryKV, который представляет собой in-memory
// key-value хранилище. Данные внутри него хранятся в обычном
// ассоциативном массиве. Интерфейс этого класса совпадает с
// FileKV за исключением конструктора. Конструктор InMemoryKV
// принимает на вход массив, который становится начальным
// значением базы данных.



class InMemoryKV
{
    private $map;

    public function __construct($initial = [])
    {
        $this->map = $initial;
    }

    public function set($key, $value)
    {
        $this->map[$key] = $value;
    }

    public function get($key, $default = null)
    {
        return $this->map[$key] ?? $default;
    }

    public function unset($key)
    {
        unset($this->map[$key]);
    }

    public function toArray()
    {
        return $this->map;
    }

}

