<?php

namespace Php\Package\ObjectOrientedDesign;

class HideEmail
{
    public function __construct(
       private string $email,
    ) {}

    public function hideEmail()
    {
        $newEmail = explode('@', $this->email);
        $name = implode('@', array_slice($newEmail, 0, count($newEmail) - 1));
        $len = floor(strlen($name) / 2);

        return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($newEmail);
    }
}

$email = new HideEmail('vanesidiot@vonz.com');
print_r($email->hideEmail());

//// to see in action:
//$emails = ['"Abc\@def"@iana.org', 'abcdlkjlkjk@hotmail.com'];
//
//foreach ($emails as $email)
//{
//    echo obfuscate_email($email) . "\n";
//}